-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Июл 01 2022 г., 12:06
-- Версия сервера: 8.0.24
-- Версия PHP: 8.0.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `ankety-anatoly`
--

-- --------------------------------------------------------

--
-- Структура таблицы `files`
--

CREATE TABLE `files` (
  `id` int NOT NULL,
  `name` text NOT NULL,
  `type` text NOT NULL,
  `size` text NOT NULL,
  `directory` text NOT NULL,
  `create-date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `files`
--

INSERT INTO `files` (`id`, `name`, `type`, `size`, `directory`, `create-date`) VALUES
(537, 'Без названия.jpg', 'image/jpeg', '7659', 'C:/OpenServer/domains/ankety-anatoly.loc/upload/20220701-094227/', '2022-07-01 02:42:27'),
(538, 'Без названия.jpg', 'image/jpeg', '7659', 'C:/OpenServer/domains/ankety-anatoly.loc/upload/resized-image/avatar/20220701-094227/', '2022-07-01 02:42:27'),
(539, 'Без названия (1).jpg', 'image/jpeg', '12294', 'C:/OpenServer/domains/ankety-anatoly.loc/upload/resized-image/20220701-094227/', '2022-07-01 02:42:27'),
(540, 'Без названия.jpg', 'image/jpeg', '7659', 'C:/OpenServer/domains/ankety-anatoly.loc/upload/resized-image/20220701-094227/', '2022-07-01 02:42:27'),
(541, 'Без названия (2).jpg', 'image/jpeg', '5408', 'C:/OpenServer/domains/ankety-anatoly.loc/upload/20220701-094650/', '2022-07-01 02:46:50'),
(542, 'Без названия (2).jpg', 'image/jpeg', '5408', 'C:/OpenServer/domains/ankety-anatoly.loc/upload/resized-image/avatar/20220701-094650/', '2022-07-01 02:46:50'),
(543, 'Без названия (2).jpg', 'image/jpeg', '5408', 'C:/OpenServer/domains/ankety-anatoly.loc/upload/resized-image/20220701-094650/', '2022-07-01 02:46:50'),
(544, 'Без названия (3).jpg', 'image/jpeg', '4975', 'C:/OpenServer/domains/ankety-anatoly.loc/upload/resized-image/20220701-094650/', '2022-07-01 02:46:50'),
(545, 'Без названия (4).jpg', 'image/jpeg', '4657', 'C:/OpenServer/domains/ankety-anatoly.loc/upload/resized-image/20220701-094650/', '2022-07-01 02:46:50'),
(546, 'Без названия (5).jpg', 'image/jpeg', '4990', 'C:/OpenServer/domains/ankety-anatoly.loc/upload/resized-image/20220701-094650/', '2022-07-01 02:46:50'),
(547, 'Без названия (6).jpg', 'image/jpeg', '4853', 'C:/OpenServer/domains/ankety-anatoly.loc/upload/resized-image/20220701-094650/', '2022-07-01 02:46:50'),
(548, 'images.jpg', 'image/jpeg', '6393', 'C:/OpenServer/domains/ankety-anatoly.loc/upload/20220701-095043/', '2022-07-01 02:50:43'),
(549, 'images.jpg', 'image/jpeg', '6393', 'C:/OpenServer/domains/ankety-anatoly.loc/upload/resized-image/avatar/20220701-095043/', '2022-07-01 02:50:44'),
(550, '4c17912d160554d7fb507fa9c3149450.jpg', 'image/jpeg', '62482', 'C:/OpenServer/domains/ankety-anatoly.loc/upload/resized-image/20220701-095043/', '2022-07-01 02:50:44'),
(551, 'Igor-Nikolaev-01.jpg', 'image/jpeg', '14195', 'C:/OpenServer/domains/ankety-anatoly.loc/upload/resized-image/20220701-095043/', '2022-07-01 02:50:44'),
(552, 'images.jpg', 'image/jpeg', '6393', 'C:/OpenServer/domains/ankety-anatoly.loc/upload/resized-image/20220701-095043/', '2022-07-01 02:50:44'),
(553, 'avatarka.jpg', 'image/jpeg', '6160', 'C:/OpenServer/domains/ankety-anatoly.loc/upload/20220701-100438/', '2022-07-01 03:04:38'),
(554, 'avatarka.jpg', 'image/jpeg', '6160', 'C:/OpenServer/domains/ankety-anatoly.loc/upload/resized-image/avatar/20220701-100438/', '2022-07-01 03:04:38'),
(555, '1572612050_1.jpg', 'image/jpeg', '717170', 'C:/OpenServer/domains/ankety-anatoly.loc/upload/resized-image/20220701-100438/', '2022-07-01 03:04:38'),
(556, 'big-family-making-selfie-background-3147302.jpg', 'image/jpeg', '123276', 'C:/OpenServer/domains/ankety-anatoly.loc/upload/resized-image/20220701-100438/', '2022-07-01 03:04:38'),
(557, 'lake-4k-background-desktop-wallpaper-preview.jpg', 'image/jpeg', '101786', 'C:/OpenServer/domains/ankety-anatoly.loc/upload/resized-image/20220701-100438/', '2022-07-01 03:04:38'),
(558, 'parizh-franciya-noch-ogni.jpg', 'image/jpeg', '308894', 'C:/OpenServer/domains/ankety-anatoly.loc/upload/resized-image/20220701-100438/', '2022-07-01 03:04:38'),
(559, 'unnamed.jpg', 'image/jpeg', '71345', 'C:/OpenServer/domains/ankety-anatoly.loc/upload/resized-image/20220701-100438/', '2022-07-01 03:04:38'),
(560, 'avatar.jpg', 'image/jpeg', '5452', 'C:/OpenServer/domains/ankety-anatoly.loc/upload/20220701-100608/', '2022-07-01 03:06:09'),
(561, 'avatar.jpg', 'image/jpeg', '5452', 'C:/OpenServer/domains/ankety-anatoly.loc/upload/resized-image/avatar/20220701-100608/', '2022-07-01 03:06:09'),
(562, '166597-shvejcariya-zrich-grossmnster-voda-oblako-3840x2160.jpg', 'image/jpeg', '834223', 'C:/OpenServer/domains/ankety-anatoly.loc/upload/resized-image/20220701-100608/', '2022-07-01 03:06:09'),
(563, '1572612050_1.jpg', 'image/jpeg', '717170', 'C:/OpenServer/domains/ankety-anatoly.loc/upload/resized-image/20220701-100609/', '2022-07-01 03:06:09'),
(564, 'apelsin-voda-zolotaya-rybka.jpg', 'image/jpeg', '38005', 'C:/OpenServer/domains/ankety-anatoly.loc/upload/resized-image/20220701-100609/', '2022-07-01 03:06:09'),
(565, 'avatar.jpg', 'image/jpeg', '5452', 'C:/OpenServer/domains/ankety-anatoly.loc/upload/resized-image/20220701-100609/', '2022-07-01 03:06:09'),
(566, 'images (1).jpg', 'image/jpeg', '9250', 'C:/OpenServer/domains/ankety-anatoly.loc/upload/resized-image/20220701-100609/', '2022-07-01 03:06:09'),
(567, 'Петров.jpg', 'image/jpeg', '12052', 'C:/OpenServer/domains/ankety-anatoly.loc/upload/20220701-100843/', '2022-07-01 03:08:43'),
(568, 'Петров.jpg', 'image/jpeg', '12052', 'C:/OpenServer/domains/ankety-anatoly.loc/upload/resized-image/avatar/20220701-100843/', '2022-07-01 03:08:43'),
(569, 'Петров.jpg', 'image/jpeg', '12052', 'C:/OpenServer/domains/ankety-anatoly.loc/upload/resized-image/20220701-100843/', '2022-07-01 03:08:43'),
(570, 'Петров2.jpg', 'image/jpeg', '7275', 'C:/OpenServer/domains/ankety-anatoly.loc/upload/resized-image/20220701-100843/', '2022-07-01 03:08:43'),
(571, 'Петров3.jpg', 'image/jpeg', '9145', 'C:/OpenServer/domains/ankety-anatoly.loc/upload/resized-image/20220701-100843/', '2022-07-01 03:08:43'),
(572, 'Петров4.jpg', 'image/jpeg', '3829', 'C:/OpenServer/domains/ankety-anatoly.loc/upload/resized-image/20220701-100843/', '2022-07-01 03:08:43'),
(573, 'Петров5.jpg', 'image/jpeg', '55321', 'C:/OpenServer/domains/ankety-anatoly.loc/upload/resized-image/20220701-100843/', '2022-07-01 03:08:43'),
(574, 'Иванов.jpg', 'image/jpeg', '4287', 'C:/OpenServer/domains/ankety-anatoly.loc/upload/20220701-100938/', '2022-07-01 03:09:38'),
(575, 'Иванов.jpg', 'image/jpeg', '4287', 'C:/OpenServer/domains/ankety-anatoly.loc/upload/resized-image/avatar/20220701-100938/', '2022-07-01 03:09:38'),
(576, 'Иванов.jpg', 'image/jpeg', '4287', 'C:/OpenServer/domains/ankety-anatoly.loc/upload/resized-image/20220701-100938/', '2022-07-01 03:09:38'),
(577, 'Иванов2.jpg', 'image/jpeg', '5394', 'C:/OpenServer/domains/ankety-anatoly.loc/upload/resized-image/20220701-100938/', '2022-07-01 03:09:38'),
(578, 'Иванов3.jpg', 'image/jpeg', '5915', 'C:/OpenServer/domains/ankety-anatoly.loc/upload/resized-image/20220701-100938/', '2022-07-01 03:09:38'),
(579, 'Петров2.jpg', 'image/jpeg', '7275', 'C:/OpenServer/domains/ankety-anatoly.loc/upload/20220701-102407/', '2022-07-01 03:24:07'),
(580, 'Петров2.jpg', 'image/jpeg', '7275', 'C:/OpenServer/domains/ankety-anatoly.loc/upload/resized-image/avatar/20220701-102407/', '2022-07-01 03:24:07'),
(581, 'lake-4k-background-desktop-wallpaper-preview.jpg', 'image/jpeg', '101786', 'C:/OpenServer/domains/ankety-anatoly.loc/upload/resized-image/20220701-102407/', '2022-07-01 03:24:07'),
(582, 'parizh-franciya-noch-ogni.jpg', 'image/jpeg', '308894', 'C:/OpenServer/domains/ankety-anatoly.loc/upload/resized-image/20220701-102407/', '2022-07-01 03:24:07'),
(583, 'zhivopisnyj-pejzazh-krasivaya-priroda.jpg', 'image/jpeg', '697398', 'C:/OpenServer/domains/ankety-anatoly.loc/upload/resized-image/20220701-102407/', '2022-07-01 03:24:07'),
(584, 'Гитара для gitarofplay.jpg', 'image/jpeg', '6892', 'C:/OpenServer/domains/ankety-anatoly.loc/upload/resized-image/20220701-102407/', '2022-07-01 03:24:07');

-- --------------------------------------------------------

--
-- Структура таблицы `photo`
--

CREATE TABLE `photo` (
  `id` int NOT NULL,
  `id_file` int NOT NULL,
  `questionnaire_id` int NOT NULL,
  `url_photo` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `photo`
--

INSERT INTO `photo` (`id`, `id_file`, `questionnaire_id`, `url_photo`) VALUES
(37, 539, 68, '/upload/resized-image/20220701-094227/Без названия (1).jpg'),
(38, 540, 68, '/upload/resized-image/20220701-094227/Без названия.jpg'),
(39, 543, 69, '/upload/resized-image/20220701-094650/Без названия (2).jpg'),
(40, 544, 69, '/upload/resized-image/20220701-094650/Без названия (3).jpg'),
(41, 545, 69, '/upload/resized-image/20220701-094650/Без названия (4).jpg'),
(42, 546, 69, '/upload/resized-image/20220701-094650/Без названия (5).jpg'),
(43, 547, 69, '/upload/resized-image/20220701-094650/Без названия (6).jpg'),
(44, 550, 70, '/upload/resized-image/20220701-095043/4c17912d160554d7fb507fa9c3149450.jpg'),
(45, 551, 70, '/upload/resized-image/20220701-095043/Igor-Nikolaev-01.jpg'),
(46, 552, 70, '/upload/resized-image/20220701-095043/images.jpg'),
(47, 555, 71, '/upload/resized-image/20220701-100438/1572612050_1.jpg'),
(48, 556, 71, '/upload/resized-image/20220701-100438/big-family-making-selfie-background-3147302.jpg'),
(49, 557, 71, '/upload/resized-image/20220701-100438/lake-4k-background-desktop-wallpaper-preview.jpg'),
(50, 558, 71, '/upload/resized-image/20220701-100438/parizh-franciya-noch-ogni.jpg'),
(51, 559, 71, '/upload/resized-image/20220701-100438/unnamed.jpg'),
(52, 562, 72, '/upload/resized-image/20220701-100608/166597-shvejcariya-zrich-grossmnster-voda-oblako-3840x2160.jpg'),
(53, 563, 72, '/upload/resized-image/20220701-100609/1572612050_1.jpg'),
(54, 564, 72, '/upload/resized-image/20220701-100609/apelsin-voda-zolotaya-rybka.jpg'),
(55, 565, 72, '/upload/resized-image/20220701-100609/avatar.jpg'),
(56, 566, 72, '/upload/resized-image/20220701-100609/images (1).jpg'),
(57, 569, 73, '/upload/resized-image/20220701-100843/Петров.jpg'),
(58, 570, 73, '/upload/resized-image/20220701-100843/Петров2.jpg'),
(59, 571, 73, '/upload/resized-image/20220701-100843/Петров3.jpg'),
(60, 572, 73, '/upload/resized-image/20220701-100843/Петров4.jpg'),
(61, 573, 73, '/upload/resized-image/20220701-100843/Петров5.jpg'),
(62, 576, 74, '/upload/resized-image/20220701-100938/Иванов.jpg'),
(63, 577, 74, '/upload/resized-image/20220701-100938/Иванов2.jpg'),
(64, 578, 74, '/upload/resized-image/20220701-100938/Иванов3.jpg'),
(65, 581, 75, '/upload/resized-image/20220701-102407/lake-4k-background-desktop-wallpaper-preview.jpg'),
(66, 582, 75, '/upload/resized-image/20220701-102407/parizh-franciya-noch-ogni.jpg'),
(67, 583, 75, '/upload/resized-image/20220701-102407/zhivopisnyj-pejzazh-krasivaya-priroda.jpg'),
(68, 584, 75, '/upload/resized-image/20220701-102407/Гитара для gitarofplay.jpg');

-- --------------------------------------------------------

--
-- Структура таблицы `questionnaire`
--

CREATE TABLE `questionnaire` (
  `id` int NOT NULL,
  `gender` text NOT NULL,
  `surname` text NOT NULL,
  `name` text,
  `patronymic` text,
  `birthday` date NOT NULL,
  `avatar` int DEFAULT NULL,
  `avatar_resized` int DEFAULT NULL,
  `color` text,
  `qualities` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `questionnaire`
--

INSERT INTO `questionnaire` (`id`, `gender`, `surname`, `name`, `patronymic`, `birthday`, `avatar`, `avatar_resized`, `color`, `qualities`) VALUES
(68, 'женский', 'Йо́вович', 'Ми́лица', 'Богда́новна', '1975-12-17', 537, 538, '#d41669', 'американская актриса, музыкант, фотомодель и модельер'),
(69, 'мужской', 'Фёдоров', 'Мирон', 'Янович', '1985-01-31', 541, 542, '#a24e2a', 'Российский хип-хоп-исполнитель, общественный деятель. Является одним из наиболее коммерчески успешных рэп-исполнителей России, его альбомы «Вечный жид» и, особенно, «Горгород» внесли значительный вклад в историю русского рэпа'),
(70, 'мужской', 'Николаев', 'Игорь', 'Юрьевич', '1960-01-17', 548, 549, '#a1e91c', 'Советский и российский композитор, певец, гитарист, пианист, музыкант, автор песен, продюсер, актёр; народный артист РФ'),
(71, 'женский', 'Девочкина', 'Мария', '', '1988-10-12', 553, 554, '#ec1818', 'Добрая, умная, красивая'),
(72, 'мужской', 'Мальчиков', 'Иван', '', '1985-12-13', 560, 561, '#0d7ec5', 'Сильный, умный, добрый'),
(73, 'мужской', 'Петров', 'Петр', 'Петрович', '1967-02-13', 567, 568, '#d5df43', 'Консервативный, образованный, утонченный'),
(74, 'мужской', 'Иванов', 'Иван', 'Иванович', '1977-01-01', 574, 575, '#000000', 'Сталинист'),
(75, 'мужской', 'Тестов', '', '', '1990-03-29', 579, 580, '#1364e7', '');

-- --------------------------------------------------------

--
-- Структура таблицы `skills`
--

CREATE TABLE `skills` (
  `id` int NOT NULL,
  `name` text NOT NULL,
  `questionnaire_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `skills`
--

INSERT INTO `skills` (`id`, `name`, `questionnaire_id`) VALUES
(71, 'самообучаемость', 68),
(72, 'трудолюбие', 68),
(73, 'усидчивость', 69),
(74, 'самообучаемость', 69),
(75, 'трудолюбие', 69),
(76, 'усидчивость', 70),
(77, 'опрятность', 70),
(78, 'усидчивость', 71),
(79, 'опрятность', 71),
(80, 'трудолюбие', 72),
(81, 'опрятность', 73),
(82, 'трудолюбие', 74);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int NOT NULL,
  `login` text NOT NULL,
  `password` text NOT NULL,
  `name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `login`, `password`, `name`) VALUES
(19, 'anatoly', '$2y$10$Vs34ZC5mN9jcpA2KHG36rOCGK9czVC6NIPVvGDw7hHb/tt8K7t50m', 'Анатолий');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `photo`
--
ALTER TABLE `photo`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `questionnaire`
--
ALTER TABLE `questionnaire`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `skills`
--
ALTER TABLE `skills`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `id_2` (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `files`
--
ALTER TABLE `files`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=585;

--
-- AUTO_INCREMENT для таблицы `photo`
--
ALTER TABLE `photo`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT для таблицы `questionnaire`
--
ALTER TABLE `questionnaire`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;

--
-- AUTO_INCREMENT для таблицы `skills`
--
ALTER TABLE `skills`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=84;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
