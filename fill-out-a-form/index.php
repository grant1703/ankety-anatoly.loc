<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/core/header.php';
global $acceptRequest;
?>
    <div class="container">
    <h1>Форма для заполнения</h1>
<?php
if ($acceptRequest['fil-out-a-form']['status']) {
    ?>
    <div class="form mt-100 success">
        Форма успешно заполнена! <br>
        <a href="/admin/questionnaires-list/">Журнал анкет</a>
    </div>
    <?php
} else {
    ?>
    <form name="questionnaire" enctype="multipart/form-data" action="" method="post" class="form mt-100"
          onsubmit="">
        <input type="hidden" name="id_form" value="fil-out-a-form">
        <?php
        $fields = $acceptRequest['fil-out-a-form']['fields'];
        if ($acceptRequest['fil-out-a-form']['status'] === false && !empty($acceptRequest['fil-out-a-form'])) {
            ?>
            <div class="error-massage">
                Проверьте все ли верно вы заполнили!
            </div>
            <?php
        }
        ?>
        <h3>Заполните пожалуйста анкету:</h3><br>
        <div id="step_1">
            <label>
                <b>Ваш пол*</b>
                <select name="gender" required class="input">
                    <?php
                    $male = $female = $empty = '';
                    if ($fields['gender']) {
                        switch ($fields['gender']) {
                            case 'мужской':
                                $male = 'selected';
                                break;
                            case 'женский':
                                $female = 'selected';
                                break;
                            default:
                                $empty = 'selected';
                        }
                    } else {
                        $empty = 'selected';
                    }
                    ?>
                    <option value="" <?= $empty ?> disabled></option>
                    <option value="мужской" <?= $male ?>>мужской</option>
                    <option value="женский" <?= $female ?>>женский</option>
                </select>
            </label>
            <label>
                <b>Фамилия*</b>
                <input type="text" name="surname" required class="input" value="<?= $fields['surname'] ?>">
            </label>
            <label>
                <b>Имя</b>
                <input type="text" name="name" class="input" value="<?= $fields['name'] ?>">
            </label>
            <label>
                <b>Отчество</b>
                <input type="text" name="patronymic" class="input" value="<?= $fields['patronymic'] ?>">
            </label>
            <label>
                <b>Дата рождения*</b>
                <input type="date" name="birthday" required class="input" value="<?= $fields['birthday'] ?>">
            </label>
            <div id="next_1" class="next">Далее</div>
        </div>
        <div id="step_2" class="none">
            <label>
                <b>Ваш аватар</b> <br>
                <small>(не более 100 Кб, доступные форматы: *.png, *.jpg/jpeg)</small>
                <input type="file" name="avatar" class="input">
            </label>
            <label>
                <b>Ваш любимый цвет</b>
                <input type="color" name="color" class="input w-50" value="<?= $fields['color'] ?>">
            </label>
            <div class="container-next-previous">
                <div id="previous_1">Назад</div>
                <div id="next_2">Далее</div>
            </div>
        </div>
        <script>
            classStepQuestionnaire.onClickPrevious(2)
            classStepQuestionnaire.onClickNext(1)
        </script>
        <div id="step_3" class="none">
            <label>
                <b>Ваши личные качества</b>
                <input type="text" name="qualities" class="input" value="<?= $fields['qualities'] ?>">
            </label>
            <div>
                <?php
                $perseverance = $neatness = $selfLearning = $industriousness = '';
                if ($fields['skills']) {
                    foreach ($fields['skills'] as $skill) {
                        switch ($skill) {
                            case 'усидчивость':
                                $perseverance = 'checked';
                                break;
                            case 'опрятность':
                                $neatness = 'checked';
                                break;
                            case 'самообучаемость':
                                $selfLearning = 'checked';
                                break;
                            case 'трудолюбие':
                                $industriousness = 'checked';
                        }
                    }
                }
                ?>
                <b>Ваши навыки</b>
                <div class="input">
                    <input type="checkbox" id="skills_1" name="skills[]"
                           value="усидчивость" <?= $perseverance ?>>
                    <label for="skills_1">усидчивость</label>
                </div>
                <div class="input">
                    <input type="checkbox" id="skills_2" name="skills[]"
                           value="опрятность" <?= $neatness ?>>
                    <label for="skills_2">опрятность</label>
                </div>
                <div class="input">
                    <input type="checkbox" id="skills_3" name="skills[]"
                           value="самообучаемость" <?= $selfLearning ?>>
                    <label for="skills_3">самообучаемость</label>
                </div>
                <div class="input">
                    <input type="checkbox" id="skills_4" name="skills[]"
                           value="трудолюбие" <?= $industriousness ?>>
                    <label for="skills_4">трудолюбие</label>
                </div>
            </div>
            <div class="container-next-previous">
                <div id="previous_2">Назад</div>
                <div id="next_3">Далее</div>
            </div>
        </div>
        <script>
            classStepQuestionnaire.onClickPrevious(3)
            classStepQuestionnaire.onClickNext(2)
        </script>
        <div id="step_4" class="none">
            <label>
                <b>Загрузите ваши фотографии</b> <br>
                <small>(макс. 5 фото, каждая не более 1 Мб, форматы: *.png, *.jpg/jpeg)</small>
                <input type="file" name="photo[]" multiple class="input">
            </label>
            <input name="submit" type="submit" value="Отправить" class="submit">
            <div class="container-next-previous">
                <div id="previous_3">Назад</div>
            </div>
        </div>
        <script>
            classStepQuestionnaire.onClickPrevious(4)
            classStepQuestionnaire.onClickNext(3)
            classStepQuestionnaire.onclickSubmit()
            classStepQuestionnaire.cleanSubmit()
        </script>
    </form>
    </div>
    <?php
}
require_once $_SERVER['DOCUMENT_ROOT'].'/core/footer.php';