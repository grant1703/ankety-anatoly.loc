<?php

namespace Core;

class Redirect
{
    public static function truncateIndex()
    {
        $withoutGet = explode("?", $_SERVER['REQUEST_URI'])[0];
        $getParameters = explode("?", $_SERVER['REQUEST_URI'])[1];
        $arCurrentUrl = explode("/", $withoutGet);
        $keyLastItem = count($arCurrentUrl) - 1;
        if ($arCurrentUrl[$keyLastItem] == 'index.php') {
            unset($arCurrentUrl[$keyLastItem]);
            $newUrl = implode("/", $arCurrentUrl).'?'.$getParameters;
            header("Location: $newUrl");
            die();
        }
    }
}