<?php

namespace Core;

class Files extends DBConnTemplate
{
    const DIR_FOR_FILES            = '/upload/';
    const FOLDER_FOR_RESIZED_IMAGE = 'resized-image';

    protected function getTableName()
    {
        return 'files';
    }

    public function processing()
    {
        $arFields = self::saveNewFiles();
        $arResultFiles = [];
        foreach ($arFields as $nameField => $arFiles) {
            foreach ($arFiles as $arFile) {
                $arResultFiles[$nameField][] = $this->add(
                    [
                        'name'      => $arFile['name'],
                        'type'      => $arFile['type'],
                        'size'      => $arFile['size'],
                        'directory' => $arFile['directory']
                    ]
                );
            }
        }
        return $arResultFiles;
    }

    public static function saveNewFiles(): bool|array
    {
        $arFields = self::getArrayFiles();
        $arResult = [];
        if ($arFields) {
            foreach ($arFields as $nameField => $arFiles) {
                foreach ($arFiles as $arFile) {
                    if ($nameField == 'avatar') {
                        $sDirectoryResized = self::createFolder(
                            self::FOLDER_FOR_RESIZED_IMAGE.'/avatar/'.date('Ymd-His')
                        );
                        $sDirectory = self::createFolder(date('Ymd-His'));
                        move_uploaded_file($arFile['tmp_name'], $sDirectory.$arFile['name']);
                        $arFile['directory'] = $sDirectory;
                        $arResult[$nameField][] = $arFile;
                        $arFile['directory'] = self::resizeImage($arFile, $sDirectoryResized, 60, 60);
                    } else {
                        $sDirectoryResized = self::createFolder(
                            self::FOLDER_FOR_RESIZED_IMAGE.'/'.date('Ymd-His')
                        );
                        $arFile['directory'] = self::resizeImage($arFile, $sDirectoryResized, 600, 700, false);
                    }
                    $arResult[$nameField][] = $arFile;
                }
            }
        } else {
            $arResult = false;
        }
        return $arResult;
    }

    public static function getArrayFiles(): bool|array
    {
        if ($_FILES) {
            $arFiles = [];
            foreach ($_FILES as $nameFileField => $arField) {
                foreach ($arField as $nameProperty => $propertyFile) {
                    if (is_array($propertyFile)) {
                        foreach ($propertyFile as $keyFile => $valueFile) {
                            $arFiles[$nameFileField][$keyFile][$nameProperty] = $valueFile;
                        }
                    } else {
                        $arFiles[$nameFileField][0][$nameProperty] = $propertyFile;
                    }
                }
            }
            return $arFiles;
        }
        return false;
    }

    public static function createFolder($folder): string
    {
        $dir = $_SERVER['DOCUMENT_ROOT'].self::DIR_FOR_FILES.$folder;
        if (!file_exists($dir)) {
            mkdir($dir, 0755, true);
        }
        return $dir.'/';
    }

    public function getUrlFile($id): string
    {
        $arFile = $this->getById($id)[0];
        $sDirectory = explode('upload', $arFile['directory'])[1];
        return "/upload$sDirectory{$arFile['name']}";
    }

    private static function resizeImage($arImg, $newDirectory, $newWidth, $newHeight, $allSide = true): bool|string
    {
        $fileType = $arImg['type'];
        $originalFile = $arImg['tmp_name'];
        if (!file_exists($originalFile)) {
            $originalFile = $arImg['directory'].$arImg['name'];
        } else {
            move_uploaded_file($originalFile, $newDirectory.$arImg['name']);
            $originalFile = $newDirectory.$arImg['name'];
        }
        $resizedFile = $newDirectory.$arImg['name'];
        $originalImage = false;
        if ($fileType == 'image/png') {
            $originalImage = imagecreatefrompng($originalFile);
        } elseif ($fileType == 'image/jpeg') {
            $originalImage = imagecreatefromjpeg($originalFile);
        }
        if ($originalImage) {
            $oldX = imagesx($originalImage);
            $oldY = imagesy($originalImage);
            if ($oldX > $newWidth || $oldY > $newHeight) {
                $x = 0;
                $y = 0;

                if ($allSide) {
                    $width = $newWidth;
                    $height = $newHeight;

                    if ($oldX > $oldY) {
                        $x = ceil(($oldX - $oldY) / 2);
                        $oldX = $oldY;
                    } elseif ($oldY > $oldX) {
                        $y = ceil(($oldY - $oldX) / 2);
                        $oldY = $oldX;
                    }
                } else {
                    if ($oldX > $oldY) {
                        $width = $newWidth;
                        $height = $oldY * $newWidth / $oldX;
                    } else {
                        $width = ceil($oldX) * ceil($newHeight) / ceil($oldY);
                        $height = $newHeight;
                    }
                }


                $newImage = imagecreatetruecolor($width, $height);
                if ($fileType == 'image/png') {
                    imagealphablending($newImage, false);
                    imagesavealpha($newImage, true);
                }
                imagecopyresampled($newImage, $originalImage, 0, 0, $x, $y, $width, $height, $oldX, $oldY);

                if ($fileType == 'image/jpeg') {
                    imagejpeg($newImage, $resizedFile, 100);
                } elseif ($fileType == 'image/png') {
                    imagepng($newImage, $resizedFile, 5);
                }

                imagedestroy($originalImage);
                imagedestroy($newImage);
                $srcNewFile = $newDirectory;
            } else {
                $srcNewFile = $newDirectory;
            }

            return $srcNewFile;
        }
        return false;
    }
}