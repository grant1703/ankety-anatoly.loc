<?php

namespace Core;

use mysqli_result;

class DBConnTemplate
{
    function __construct()
    {
        global $arSettings;
        $DB = $arSettings['DB'];
        $this->link = mysqli_connect(
            $DB['host'],
            $DB['login'],
            $DB['password'],
            $DB['dataBaseName'],
            $DB['port']
        );
        if (mysqli_connect_errno()) {
            echo 'Ошибка в подключении к базе данных';
            exit();
        }
    }

    protected function getTableName()
    {
        return '';
    }

    /**
     * Принимает массив параметров сущности с их значениями, где ключи являются наименованиями параметров, а значения - значениями параметра. Добавляет новый элемент с указанными параметрами.
     * @param array $arParameters array(ParameterName => ParameterValue)
     * @return bool|mysqli_result
     */
    public function add(array $arParameters)
    {
        $sFieldsNames = $sFieldsValues = '';
        foreach ($arParameters as $fieldName => $fieldValue) {
            $comma = '';
            if ($sFieldsNames != '') {
                $comma = ', ';
            }
            $quotes = '';
            if (is_string($fieldValue)) {
                $quotes = "'";
            }
            $sFieldsNames .= "$comma`$fieldName`";
            $sFieldsValues .= $comma.$quotes.$fieldValue.$quotes;
        }
        $sFieldsNames = '`id`, '.$sFieldsNames;
        $sFieldsValues = 'NULL, '.$sFieldsValues;
        $tableName = static::getTableName();
        $sql = "INSERT INTO `$tableName` ($sFieldsNames) VALUES ($sFieldsValues)";
        if (mysqli_query($this->link, $sql)) {
            $sql = "SELECT LAST_INSERT_ID()";
            $result = mysqli_query($this->link, $sql);
            return mysqli_fetch_all($result, MYSQLI_ASSOC)[0]['LAST_INSERT_ID()'];
        }
        return false;
    }

    /**
     * Принимает id элемента и возвращает массив его параметров.
     * @param int $iId
     * @return array
     */
    public function getById($iId)
    {
        $tableName = static::getTableName();
        $where = self::getWHERE(['id' => $iId]);
        $sql = "SELECT * FROM $tableName $where";
        $result = mysqli_query($this->link, $sql);
        return mysqli_fetch_all($result, MYSQLI_ASSOC);
    }

    /**
     * Принимает массив параметров и возвращает массив элементов по заданному фильтру, сортировки с выбранными полями, с заданным количеством.
     * @param array $arParameters array('select' => array(field1, field2...), 'filter' => array(fieldName => fieldValue), 'order' => array(field1, field2...), 'limit' => array(number => quantity))
     * @return bool|array
     */
    public function getList(array $arParameters = []): bool|array
    {
        if (!is_array($arParameters)) {
            return false;
        }
        $tableName = static::getTableName();
        $limit = '';
        if ($arParameters['select']) {
            $sParameters = 'SELECT '.implode(', ', $arParameters['select']);
        } else {
            $sParameters = 'SELECT *';
        }
        $sParameters .= ' FROM '.$tableName;
        if ($arParameters['filter']) {
            $sParameters .= ' '.self::getWHERE($arParameters['filter']);
        }
        if ($arParameters['order']) {
            $sParameters .= ' ORDER BY '.implode(', ', $arParameters['order']);
        }
        if ($arParameters['limit']) {
            foreach ($arParameters['limit'] as $number => $quantity) {
                if ($number == 1) {
                    $limit = 'LIMIT '.$quantity;
                } else {
                    $from = $number * $quantity - $quantity;
                    $limit = "LIMIT $from, $quantity";
                }
            }
            $sParameters .= ' '.$limit;
        }

        $sql = $sParameters;
        $result = mysqli_query($this->link, $sql);
        return mysqli_fetch_all($result, MYSQLI_ASSOC);
    }

    /**
     * Принимает id элемента и массив новых данных, где ключи наименования полей, а значения новые значения указанных полей.
     * @param int $iId
     * @param array $arParameters array(fieldName => fieldValue)
     * @return bool
     */
    public function updateById($iId, $arParameters)
    {
        if (!is_array($arParameters)) {
            return false;
        }
        $tableName = static::getTableName();
        $set = '';
        foreach ($arParameters as $fieldName => $fieldValue) {
            $set[] = $fieldName.'='.$fieldValue;
        }
        $set = 'SET '.implode(', ', $set);
        $where = self::getWHERE(['id' => $iId]);
        $sql = "UPDATE $tableName $set $where";
        mysqli_query($this->link, $sql);
        if (mysqli_connect_errno()) {
            return false;
        }
        return true;
    }

    /**
     * Принимает id элемента и удаляет его.
     * @param int $iId
     * @return bool|mysqli_result
     */
    public function deleteById($iId)
    {
        $tableName = static::getTableName();
        $where = self::getWHERE(['id' => $iId]);
        $sql = "DELETE FROM $tableName $where";
        $result = mysqli_query($this->link, $sql);
        if (mysqli_connect_errno()) {
            return false;
        }
        return $result;
    }

    public function getCount($arFilter = [])
    {
        $tableName = static::getTableName();
        if ($arFilter) {
            $filter = self::getWHERE($arFilter);
            $sql = "SELECT COUNT(1) FROM $tableName $filter";
        } else {
            $sql = "SELECT COUNT(1) FROM $tableName";
        }
        $result = mysqli_query($this->link, $sql);
        return count(mysqli_fetch_all($result, MYSQLI_ASSOC));
    }

    public static function getWHERE($arFilter)
    {
        $arWhere = [];
        foreach ($arFilter as $nameField => $valueField) {
            $not = $sign = '';
            $direction = mb_substr($nameField, 0, 1);
            switch ($direction) {
                case '!':
                    $not = ' NOT ';
                    $sign = ' = ';
                    $nameField = mb_substr($nameField, 1);
                    break;
                case '>':
                    $sign = ' > ';
                    $nameField = mb_substr($nameField, 1);
                    break;
                case '<':
                    $sign = ' < ';
                    $nameField = mb_substr($nameField, 1);
                    break;
                default:
                    $sign = ' = ';
            }
            if (is_array($valueField)) {
                $arWhere[] = $nameField.$not." IN ('".implode("', '", $valueField)."')";
            } else {
                (is_string($valueField)) ? $quote = "'" : $quote = '';
                $arWhere[] = $not.$nameField.$sign.$quote.$valueField.$quote;
            }
        }
        return "WHERE ".implode(" AND ", $arWhere);
    }

    public static function printVariable($variable)
    {
        echo '<pre>';
        var_dump($variable);
        echo '</pre>';
    }
}