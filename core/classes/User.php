<?php

namespace Core;

class User extends DBConnTemplate
{
    protected function getTableName()
    {
        return 'users';
    }

    public function login($login, $password)
    {
        if (empty($login) || empty($password)) {
            return false;
        }
        $arUser = $this->getList(
            [
                'filter' => ['login' => $login]
            ]
        )[0];
        if (password_verify($password, $arUser['password'])) {
            Session::add(
                [
                    'idUser'   => $arUser['id'],
                    'nameUser' => $arUser['name'],
                    'login'    => $login
                ]
            );
        } else {
            return false;
        }
        return true;
    }

    public function isLogin()
    {
        $arSession = Session::getList();
        if ($arSession['login']) {
            $arUser = $this->getById($arSession['idUser'])[0];
            if ($arUser['login'] == $arSession['login']) {
                return true;
            }
        }
        return false;
    }

    public function logout()
    {
        global $arSettings;
        $pathLoginPage = $arSettings['login']['pathPage'];
        $arParameters = ['idUser', 'nameUser', 'login'];
        foreach ($arParameters as $nameParameter) {
            Session::delete($nameParameter);
        }
        header("Location: $pathLoginPage");
    }

    public function registration($name, $login, $password)
    {
        if (empty($name) || empty($login) || empty($password)) {
            return false;
        }
        $hash = password_hash($password, PASSWORD_DEFAULT);
        return $this->add(
            [
                'login'    => $login,
                'password' => $hash,
                'name'     => $name
            ]
        );
    }

    /**
     * Возвращает данные текущего авторизованного пользователя.
     * @return array
     */
    public static function getCurrentUser(): array
    {
        return Session::getList();
    }
}