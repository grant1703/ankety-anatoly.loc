<?php

namespace Core;

class Questionnaire extends DBConnTemplate
{
    const NUMBER_OF_PROFILES_PER_PAGE = 5;
    /**
     * @var int
     */
    public int $countQuestionnaire;

    protected function getTableName(): string
    {
        return 'questionnaire';
    }

    public function getDataForTable(): array
    {
        $filter = $order = [];
        if (!$_GET['page']) {
            $page = 1;
        } else {
            $page = $_GET['page'];
        }
        if ($_GET['skills']) {
            $filter = [
                'skills' => $_GET['skills']
            ];
        }
        if ($_GET['order'] && $_GET['order'] != '') {
            $order = [$_GET['order'] => $_GET['direction']];
        }
        $arData = $this->getQuestionnaireAndSkills(
            $filter,
            $order,
            $page
        );
        $arResult = [];
        $obFiles = new Files();
        $obSkills = new Skills();
        $obPhoto = new Photo();
        $arSkills = $obSkills->getList(
            [
                'filter' => ['questionnaire_id' => array_column($arData, 'id')]
            ]
        );
        $arSkillsForQuestionnaire = $arPhotoForQuestionnaire = [];
        foreach ($arSkills as $arSkill) {
            $arSkillsForQuestionnaire[$arSkill['questionnaire_id']][] = $arSkill['name'];
        }
        $arPhoto = $obPhoto->getList(
            [
                'filter' => ['questionnaire_id' => array_column($arData, 'id')]
            ]
        );
        foreach ($arPhoto as $photo) {
            $arPhotoForQuestionnaire[$photo['questionnaire_id']][] = $photo['url_photo'];
        }
        foreach ($arData as $arElement) {
            $arElement['skills'] = $arSkillsForQuestionnaire[$arElement['id']];
            $arElement['photo'] = $arPhotoForQuestionnaire[$arElement['id']];
            if ($arElement['avatar']) {
                $arElement['avatar'] = $obFiles->getUrlFile($arElement['avatar']);
            }
            if ($arElement['avatar_resized']) {
                $arElement['avatar_resized'] = $obFiles->getUrlFile($arElement['avatar_resized']);
            }

            $arResult[] = $arElement;
        }
        return $arResult;
    }

    private function getQuestionnaireAndSkills($arFilter = [], $arOrder = [], $page = 1): array
    {
        $arParameters = [
            'select' => [
                'questionnaire.id AS id',
                'questionnaire.avatar AS avatar',
                'questionnaire.avatar_resized AS avatar_resized',
                'questionnaire.surname AS surname',
                'questionnaire.name AS name',
                'questionnaire.patronymic AS patronymic',
                'questionnaire.gender AS gender',
                'questionnaire.birthday AS birthday',
                'questionnaire.color AS color',
                'questionnaire.qualities AS qualities'
            ],
            'limit'  => [
                $page => self::NUMBER_OF_PROFILES_PER_PAGE
            ]
        ];
        $sql = 'SELECT '.implode(', ', $arParameters['select']);
        $sqlForCount = 'SELECT COUNT(1)';
        $from = " FROM questionnaire
            LEFT JOIN skills ON questionnaire.id = skills.questionnaire_id";
        $sql .= $from;
        $sqlForCount .= $from;
        if ($arFilter) {
            $sql .= ' '.self::getWHERE($arFilter);
            $sqlForCount .= ' '.self::getWHERE($arFilter);
        }
        $group = ' GROUP BY questionnaire.id';
        $sql .= $group;
        $sqlForCount .= $group;
        if ($arOrder) {
            foreach ($arOrder as $valueOrder => $directionOrder) {
                $sql .= " ORDER BY $valueOrder $directionOrder";
            }
        }
        $limit = '';
        foreach ($arParameters['limit'] as $number => $quantity) {
            if ($number == 1) {
                $limit = 'LIMIT '.$quantity;
            } else {
                $from = $number * $quantity - $quantity;
                $limit = "LIMIT $from, $quantity";
            }
        }
        $sql .= ' '.$limit;
        $sql = str_replace('skills IN', 'skills.name IN', $sql);
        $sqlForCount = str_replace('skills IN', 'skills.name IN', $sqlForCount);
        $resultForCount = mysqli_query($this->link, $sqlForCount);

        if ($resultForCount) {
            $this->countQuestionnaire = count(mysqli_fetch_all($resultForCount, MYSQLI_ASSOC));
        } else {
            $this->countQuestionnaire = 0;
        }
        $result = mysqli_query($this->link, $sql);
        return mysqli_fetch_all($result, MYSQLI_ASSOC);
    }

    public function processing($arRequest): \mysqli_result|bool
    {
        $obFiles = new Files();
        $arFiles = $obFiles->processing();
        $questionnaireId = $this->add(
            [
                'gender'         => $arRequest['gender'],
                'surname'        => $arRequest['surname'],
                'name'           => $arRequest['name'],
                'patronymic'     => $arRequest['patronymic'],
                'birthday'       => $arRequest['birthday'],
                'avatar'         => $arFiles['avatar'][0],
                'avatar_resized' => $arFiles['avatar'][1],
                'color'          => $arRequest['color'],
                'qualities'      => $arRequest['qualities']
            ]
        );
        $obSkills = new Skills();
        if ($arRequest['skills']) {
            foreach ($arRequest['skills'] as $skill) {
                $obSkills->add(
                    [
                        'name'             => $skill,
                        'questionnaire_id' => $questionnaireId
                    ]
                );
            }
        }
        $obPhoto = new Photo();
        if ($arFiles['photo']) {
            foreach ($arFiles['photo'] as $photo) {
                $obPhoto->add(
                    [
                        'id_file'          => $photo,
                        'questionnaire_id' => $questionnaireId,
                        'url_photo'        => $obFiles->getUrlFile($photo)
                    ]
                );
            }
        }
        return $questionnaireId;
    }

    public function getArrayPage(): array
    {
        $countPage = ceil($this->countQuestionnaire / self::NUMBER_OF_PROFILES_PER_PAGE);
        $arResult = [];
        for ($i = 1; $i <= $countPage; $i++) {
            $class = '';
            if ($_GET['page'] == $i) {
                $class = ' class="page-active"';
            }
            $arResult[] = [
                'name'  => $i,
                'id'    => "page_$i",
                'class' => $class
            ];
        }
        return $arResult;
    }

    public function getQuestionnaireById($id)
    {
        if ($id) {
            $arQuestionnaire = $this->getById($id)[0];
            $obFile = new Files();
            $arQuestionnaire['avatar'] = $obFile->getUrlFile($arQuestionnaire['avatar']);
            $obSkills = new Skills();
            $arQuestionnaire['skills'] = array_column(
                $obSkills->getList(
                    [
                        'filter' => [
                            'questionnaire_id' => $id
                        ]
                    ]
                ),
                'name'
            );
            $obPhoto = new Photo();
            $arQuestionnaire['photo'] = array_column(
                $obPhoto->getList(
                    [
                        'filter' => [
                            'questionnaire_id' => $id
                        ]
                    ]
                ),
                'url_photo'
            );
            return $arQuestionnaire;
        } else {
            return false;
        }
    }
}