<?php
$classesList = [
    'DBConnTemplate.php',
    'Files.php',
    'Questionnaire.php',
    'Session.php',
    'User.php',
    'Redirect.php',
    'Request.php',
    'Skills.php',
    'Photo.php'
];
foreach ($classesList as $class) {
    require_once __DIR__.'/'.$class;
}