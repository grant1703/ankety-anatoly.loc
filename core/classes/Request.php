<?php

namespace Core;

class Request
{
    public static function acceptRequest(): array
    {
        global $arSettings;
        $result = [];
        $arRequest = [];
        if ($_POST) {
            $request = $_POST;
            $obUser = new User();
            $stringRequest = '';
            if (is_array($request)) {
                foreach ($request as $keyRequest => $valueRequest) {
                    if (is_array($valueRequest)) {
                        foreach ($valueRequest as $item) {
                            $arRequest[$keyRequest][] = self::cleanValue($item);
                        }
                    } else {
                        $arRequest[$keyRequest] = self::cleanValue($valueRequest);
                    }
                }
            } else {
                $stringRequest = self::cleanValue($request);
            }
            switch ($arRequest['id_form']) {
                case 'login':
                    $result['login']['status'] = $obUser->login($arRequest['login'], $arRequest['password']);
                    break;
                case 'registration':
                    $result['registration']['status'] = $obUser->registration($arRequest['name'], $arRequest['login'], $arRequest['password']);
                    break;
                case 'fil-out-a-form':
                    $obQuestionnaire = new Questionnaire();
                    $result[$arRequest['id_form']]['status'] = $obQuestionnaire->processing($arRequest);
                    break;
                default:
                    $result[$arRequest['id_form']]['status'] = false;
            }
        }
        foreach ($result as $key => $item) {
            if (!$item['status']) {
                $result[$key]['fields'] = $arRequest;
            } else {
                foreach ($arRequest as $fieldName => $fieldValue) {
                    $result[$key]['fields'][$fieldName] = '';
                }
            }
        }
        return $result;
    }

    protected static function cleanValue($value = ""): array|string
    {
        if (!is_file($value) || !is_array($value)) {
            $value = trim($value);
            $value = stripslashes($value);
            $value = strip_tags($value);
            return htmlspecialchars($value);
        }
        return $value;
    }
}