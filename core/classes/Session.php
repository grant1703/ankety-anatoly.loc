<?php

namespace Core;

class Session
{
    /**
     * Принимает массив параметров, где ключи - это наименования параметров, а их значения - это значения параметров.
     * @param array $arParameters array(nameParameter => valueParameter)
     * @return bool
     */
    public static function add($arParameters)
    {
        if (!is_array($arParameters)) {
            return false;
        }
        foreach ($arParameters as $nameParameter => $valueParameter) {
            $_SESSION[$nameParameter] = $valueParameter;
        }
        return true;
    }

    /**
     * Возвращает все параметры, хранящиеся в сессии.
     * @return array
     */
    public static function getList()
    {
        return $_SESSION;
    }

    public static function delete($nameParameter): void
    {
        unset($_SESSION[$nameParameter]);
    }
}