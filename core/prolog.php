<?php
session_start();

require_once __DIR__.'/.settings.php';
require_once __DIR__.'/classes/requireClasses.php';
require_once __DIR__.'/php_interface/init.php';

use Core\User;
use Core\Redirect;
use Core\Request;
use Core\Questionnaire;

global $arSettings;
$arProtectedPages = $arSettings['protected_pages'];

Redirect::truncateIndex();

$currentPage = explode('?', $_SERVER['REQUEST_URI'])[0];
if (in_array($currentPage, $arProtectedPages)) {
    $obUser = new User();
    if (!$obUser->isLogin()) {
        $obUser->logout();
    }
}
$acceptRequest = Request::acceptRequest();
if ($_SERVER['REQUEST_URI'] == '/admin/questionnaires-list/') {
    header('Location: /admin/questionnaires-list/?page=1');
}
if ($_SERVER['SCRIPT_NAME'] == '/admin/questionnaires-list/index.php') {
    $obQuestionnaire = new Questionnaire();
    $arQuestionnaireList = $obQuestionnaire->getDataForTable();
    $arPagination = $obQuestionnaire->getArrayPage();
}
if ($_SERVER['SCRIPT_NAME'] == '/admin/questionnaire/index.php') {
    $obQuestionnaire = new Questionnaire();
    $arQuestionnaire = $obQuestionnaire->getQuestionnaireById($_GET['id']);
}