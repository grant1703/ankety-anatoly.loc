<?php
$arSettings = [
    'DB'              => [
        'host'         => 'localhost',
        'dataBaseName' => 'ankety-anatoly',
        'login'        => 'root',
        'password'     => '',
        'port'         => 3306
    ],
    'login'           => [
        'pathPage' => '/login/'
    ],
    'protected_pages' => [
        '/admin/questionnaire/',
        '/admin/questionnaires-list/'
    ]
];