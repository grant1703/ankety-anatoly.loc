<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/core/header.php';
global $acceptRequest, $arSettings;
$user = new \Core\User();
?>
    <div class="container">
    <h1>Регистрация</h1>
<?php
if ($acceptRequest['registration']['status']) {
    ?>
    <div class="form mt-100 success">
        Регистрация прошла успешно! Авторизуйтесь пожалуйста.
    </div>
    <?php
} else {
    ?>
    <form action="" method="post" class="form mt-100">
        <input type="hidden" name="id_form" value="registration">
        <?
        $fields = $acceptRequest['registration']['fields'];
        if ($acceptRequest['registration']['status'] === false && !empty($acceptRequest['registration'])) {
            ?>
            <div class="error-massage">
                Проверьте все ли верно вы заполнили!
            </div>
            <?php
        }
        ?>
        <h3>Введите данные для регистрации:</h3><br>
        <label>
            Имя
            <input type="text" name="name" class="input" value="<?= $fields['name'] ?>">
        </label>
        <label>
            Логин
            <input type="text" name="login" class="input" value="<?= $fields['login'] ?>">
        </label>
        <label>
            Пароль
            <input type="text" name="password" class="input">
        </label><br>
        <input type="submit" value="Зарегистрироваться" class="submit">
    </form>
    </div>
    <?php
}
require_once $_SERVER['DOCUMENT_ROOT'].'/core/footer.php';