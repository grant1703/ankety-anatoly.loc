<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/core/header.php';
global $acceptRequest, $arSettings;
$user = new \Core\User();
?>
    <div class="container">
    <h1>Авторизация</h1>
<?php
if ($user->isLogin()) {
    ?>
    <div class="form mt-100 success">
        Авторизация прошла успешно!
    </div>
    <?php
} else {
    ?>
    <form action="" method="post" class="form mt-100">
        <input type="hidden" name="id_form" value="login">
        <?
        $fields = $acceptRequest['login']['fields'];
        if ($acceptRequest['login']['status'] === false && !empty($acceptRequest['login'])) {
            ?>
            <div class="error-massage">
                Проверьте все ли верно вы заполнили!
            </div>
            <?php
        }
        ?>
        <h3>Введите логин и пароль:</h3><br>
        <label>
            Логин
            <input type="text" name="login" class="input" value="<?= $fields['login'] ?>">
        </label>
        <label>
            Пароль
            <input type="password" name="password" class="input">
        </label>
        <input type="submit" value="Войти" class="submit">
        <a href="/login/registration.php">Регистрация</a>
    </form>
    </div>
    <?php
}
require_once $_SERVER['DOCUMENT_ROOT'].'/core/footer.php';