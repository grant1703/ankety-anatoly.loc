<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/core/header.php';
global $arQuestionnaireList, $arPagination;
$fields = [];
if (
    $_GET['birthday']
    || $_GET['surname']
    || $_GET['skills']
    || $_GET['order']
    || $_GET['direction']
) {
    $fields = $_GET;
}
?>
    <div class="container"><h1>Журнал анкет</h1></div>

<?php
$perseverance = $neatness = $selfLearning = $industriousness = $surname = $birthday = $ASC = $DESC = '';
if ($fields['skills']) {
    foreach ($fields['skills'] as $skill) {
        switch ($skill) {
            case 'усидчивость':
                $perseverance = 'checked';
                break;
            case 'опрятность':
                $neatness = 'checked';
                break;
            case 'самообучаемость':
                $selfLearning = 'checked';
                break;
            case 'трудолюбие':
                $industriousness = 'checked';
        }
    }
}
if ($fields['order']) {
    switch ($fields['order']) {
        case 'surname':
            $surname = 'selected';
            break;
        case 'birthday':
            $birthday = 'selected';
    }
}
if ($fields['direction']) {
    switch ($fields['direction']) {
        case 'ASC':
            $ASC = 'selected';
            break;
        case 'DESC':
            $DESC = 'selected';
    }
}
?>
    <div class="mt-50"></div>
    <div class="table-settings">
        <form action="" method="get" enctype="multipart/form-data">
            <b>Навыки:</b>
            <input type="hidden" name="page" value="<?= $_GET['page'] ?>">
            <input type="checkbox" id="skills_1" name="skills[]"
                   value="усидчивость" <?= $perseverance ?>>
            <label for="skills_1">усидчивость |</label>
            <input type="checkbox" id="skills_2" name="skills[]"
                   value="опрятность" <?= $neatness ?>>
            <label for="skills_2">опрятность |</label>
            <input type="checkbox" id="skills_3" name="skills[]"
                   value="самообучаемость" <?= $selfLearning ?>>
            <label for="skills_3">самообучаемость |</label>
            <input type="checkbox" id="skills_4" name="skills[]"
                   value="трудолюбие" <?= $industriousness ?>>
            <label for="skills_4">трудолюбие</label>
            <b> | Сортировка:</b>
            <label for="order"></label>
            <select name="order" id="order">
                <option value=""></option>
                <option value="surname" <?= $surname ?>>Фамилия</option>
                <option value="birthday" <?= $birthday ?>>День рождения</option>
            </select>
            <label for="direction"></label>
            <select name="direction" id="direction">
                <option value="ASC" <?= $ASC ?>>a-z(0-9)</option>
                <option value="DESC" <?= $DESC ?>>z-a(9-0)</option>
            </select>
            <input type="submit" class="submit-filter" value="Применить">
        </form>
        <div class="pagination">
            <? foreach ($arPagination as $arPage): ?>
                <span<?= $arPage['class'] ?> id="<?= $arPage['id'] ?>"><?= $arPage['name'] ?></span>
                <script>
                    classOnClickRedirect.onClick('<?= $arPage['id'] ?>')
                </script>
            <? endforeach; ?>
        </div>
    </div>
    <table class="table" style="margin-top: 30px">
        <tr>
            <th>#</th>
            <th>Аватар</th>
            <th>Фамилия</th>
            <th>Имя</th>
            <th>Отчество</th>
            <th>Пол</th>
            <th>День рождения</th>
            <th>Любимый цвет</th>
            <th>Личные качества</th>
            <th>Навыки</th>
            <th>Фото</th>
        </tr>
        <?php
        if (!$arQuestionnaireList) {
            echo '<tr><td class="no-data" colspan="11">';
            echo '<center><h1>Ничего не найдено...</h1></center>';
            echo '</td></tr>';
        } else {
            foreach ($arQuestionnaireList as $arQuestionnaire) {
                ?>
                <tr>
                    <td><?= $arQuestionnaire['id'] ?></td>
                    <td>
                        <a href="http://ankety-anatoly.loc/admin/questionnaire/?id=<?= $arQuestionnaire['id'] ?>">
                            <img src="<?= $arQuestionnaire['avatar_resized'] ?>"
                                 alt="<?= "{$arQuestionnaire['surname']} {$arQuestionnaire['name']} {$arQuestionnaire['patronymic']}" ?>">
                        </a>
                    </td>
                    <td><a href="http://ankety-anatoly.loc/admin/questionnaire/?id=<?= $arQuestionnaire['id'] ?>"
                           class="name"><?= $arQuestionnaire['surname'] ?></a></td>
                    <td><a href="http://ankety-anatoly.loc/admin/questionnaire/?id=<?= $arQuestionnaire['id'] ?>"
                           class="name"><?= $arQuestionnaire['name'] ?></a></td>
                    <td><a href="http://ankety-anatoly.loc/admin/questionnaire/?id=<?= $arQuestionnaire['id'] ?>"
                           class="name"><?= $arQuestionnaire['patronymic'] ?></a></td>
                    <td><?= $arQuestionnaire['gender'] ?></td>
                    <td><?= $arQuestionnaire['birthday'] ?></td>
                    <td>
                        <div class="color" style="background-color: <?= $arQuestionnaire['color'] ?>"></div>
                    </td>
                    <td><?= $arQuestionnaire['qualities'] ?></td>
                    <td>
                        <ul>
                            <?php foreach ($arQuestionnaire['skills'] as $skill): ?>
                                <li><?= $skill ?></li>
                            <?php endforeach; ?>
                        </ul>
                    </td>
                    <td>
                        <ul>
                            <?php foreach ($arQuestionnaire['photo'] as $photo): ?>
                                <li>
                                    <a href="<?= $photo ?>"><?= $photo ?></a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </td>
                </tr>
                <?php
            }
        }
        ?>
    </table>
    <div class="mt-100"></div>
<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/core/footer.php';