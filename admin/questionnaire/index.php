<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/core/header.php';
global $arQuestionnaire;
$arNameQuestionnaire = [
    $arQuestionnaire['surname'],
    $arQuestionnaire['name'],
    $arQuestionnaire['patronymic']
];
$arName = [];
foreach ($arNameQuestionnaire as $name) {
    if ($name != '') {
        $arName[] = $name;
    }
}
$nameQuestionnaire = implode(' ', $arName);
$comeBack = '/admin/questionnaires-list/?page=1';
if ($_SERVER['HTTP_REFERER']) {
    $comeBack = $_SERVER['HTTP_REFERER'];
}
?>
    <div class="container">
        <h1>Анкета № <?= $arQuestionnaire['id'] ?> на имя "<?= $nameQuestionnaire ?>"</h1>
        <div class="mt-50"></div>
        <div class="questionnaire">
            <img src="<?= $arQuestionnaire['avatar'] ?>" alt="<?= $nameQuestionnaire ?>">
            <table>
                <tr>
                    <td>Имя</td>
                    <td><?= $nameQuestionnaire ?></td>
                </tr>
                <tr>
                    <td>Пол</td>
                    <td><?= $arQuestionnaire['gender'] ?></td>
                </tr>
                <tr>
                    <td>День рождения</td>
                    <td><?= $arQuestionnaire['birthday'] ?></td>
                </tr>
                <tr>
                    <td>Любимый цвет</td>
                    <td>
                        <div class="color" style="background-color: <?= $arQuestionnaire['color'] ?>"></div>
                    </td>
                </tr>
                <tr>
                    <td>Личные качества</td>
                    <td><?= $arQuestionnaire['qualities'] ?></td>
                </tr>
                <tr>
                    <td>Навыки</td>
                    <td>
                        <ul>
                            <?php foreach ($arQuestionnaire['skills'] as $skill): ?>
                                <li><?= $skill ?></li>
                            <?php endforeach; ?>
                        </ul>
                    </td>
                </tr>
            </table>
        </div>
        <a href="<?= $comeBack ?>" class="come-back">Вернуться в журнал анкет</a>
        <h3 class="mt-50">Галерея фото:</h3>
    </div>
    <div class="gallery">
        <? foreach ($arQuestionnaire['photo'] as $urlPhoto): ?>
            <div>
                <img src="<?= $urlPhoto ?>" alt="<?= $nameQuestionnaire ?>">
            </div>
        <? endforeach; ?>
    </div>
<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/core/footer.php';