<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/template/css/style.css">
    <link rel="stylesheet" href="/template/css/clearn.css">
    <script src="/template/js/onClickRedirect.js"></script>
    <script src="/template/js/stepsQuestionnaire.js"></script>
    <title>Тестовое задание</title>
</head>
<body>
<header class="header">
    <h1><a href="/">Тестовое задание по заполнению анкет</a></h1>
    <?
    include $_SERVER['DOCUMENT_ROOT'].'/template/login.php'
    ?>
</header>
