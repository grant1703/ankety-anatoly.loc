class onClickRedirect {
    onClick = (idElement) => {
        let element = document.querySelector('#' + idElement)
        let getParameters = window.location.search.replace('?', '').split('&')
        let getNoPage = ''
        getParameters.forEach(
            (parameter) => {
                if (!parameter.includes('page')) {
                    getNoPage = getNoPage + '&' + parameter
                }
            }
        )
        element.onclick = () => {
            let newPage = idElement.split('_')
            window.location.href = '?' + newPage[0] + '=' + newPage[1] + getNoPage;
        }
    }
}

let classOnClickRedirect = new onClickRedirect()