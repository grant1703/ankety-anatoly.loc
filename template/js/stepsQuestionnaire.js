class stepsQuestionnaire {

    onClickNext = (numberStep) => {
        let numberNextStep = numberStep + 1
        let next = document.querySelector('#next_' + numberStep)
        let step1 = document.querySelector('#step_' + numberStep)
        let step2 = document.querySelector('#step_' + numberNextStep)
        next.onclick = () => {
            if (this.validation(numberStep)) {
                step1.className = 'none'
                step2.className = ''
            }
        }
    }
    onClickPrevious = (numberStep) => {
        let numberPreviousStep = numberStep - 1
        let previous = document.querySelector('#previous_' + numberPreviousStep)
        let step1 = document.querySelector('#step_' + numberStep)
        let step2 = document.querySelector('#step_' + numberPreviousStep)
        previous.onclick = () => {
            step1.className = 'none'
            step2.className = ''
        }
    }
    validation = (numberStep) => {
        if (numberStep === 1) {
            let gender = document.forms['questionnaire']['gender'].value
            let surname = document.forms['questionnaire']['surname'].value
            let birthday = document.forms['questionnaire']['birthday'].value
            if (gender === '') {
                alert('Поле Ваш пол* является обязательным для заполнения!')
                return false
            }
            if (surname === '') {
                alert('Поле Фамилия* является обязательным для заполнения!')
                return false
            }
            if (birthday === '') {
                alert('Поле Дата рождения* является обязательным для заполнения!')
                return false
            }
        }
        if (numberStep === 2) {
            let avatar = document.forms['questionnaire']['avatar'].files
            if (avatar.length !== 0) {
                if (avatar[0].size > 102400) {
                    alert('Файл для аватара не должен превышать 100 Кб! Выберите пожалуйста другой файл.')
                    return false
                }
                if (avatar[0].type !== 'image/jpeg' && avatar[0].type !== 'image/png') {
                    alert('Файл для аватара должен быть форматов: *.png, *.jpg/jpeg! Выберите пожалуйста другой файл.')
                    return false
                }
            }
        }
        if (numberStep === 4) {
            let photo = document.forms['questionnaire']['photo[]'].files
            if (photo.length !== 0) {
                if (photo.length > 5) {
                    return 'alert(\'Вы не можете загрузить более 5 фотографий!\');return false'
                }
                for (let i = 0; i < photo.length; i++) {
                    if (photo[i].size > 1000000) {
                        return 'alert(\'Вы не можете загрузить фотографии более 1 Мб весом!\');return false'
                    }
                    if (photo[i].type !== 'image/jpeg' && photo[i].type !== 'image/png') {
                        return 'alert(\'Файл для фото должен быть форматов: *.png, *.jpg/jpeg! Выберите пожалуйста другой файл.\');return false'
                    }
                }
            }
        }
        return true
    }
    onclickSubmit = () => {
        let form = document.forms['questionnaire']
        form['submit'].onclick = () => {
            form.attributes.onsubmit.value = this.validation(4)
        }
    }
    cleanSubmit = () => {
        let form = document.forms['questionnaire']
        addEventListener('input', function (event) {
            form.attributes.onsubmit.value = ''
        })
    }
}

let classStepQuestionnaire = new stepsQuestionnaire()