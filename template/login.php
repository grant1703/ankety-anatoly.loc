<?php

use Core\User;

$obUser = new User();
if ($obUser->isLogin()) {
    $arUser = User::getCurrentUser();
    echo '<div>';
    echo "<h3 class='name-user'>";
    echo $arUser['nameUser'];
    echo "</h3>";
    echo "<a href='/login/logout.php' class='come-in'>Выйти</a>";
    echo '</div>';
} else {
    global $arSettings;
    $loginPages = $arSettings['login']['pathPage'];
    ?>
    <a href="<?= $loginPages ?>" class="come-in">Войти</a>
    <?php
}