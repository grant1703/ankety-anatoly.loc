<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/core/header.php';
?>
    <div class="container">
        <h1>Главная страница</h1>

        <div class="container">
            <div class="row">
                <a class="card" href="/fill-out-a-form/">
                    <div class="title">Заполнить анкету</div>
                </a>
                <a class="card" href="/admin/questionnaires-list/">
                    <div class="title">Журнал анкет</div>
                </a>
            </div>
        </div>
    </div>
<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/core/footer.php';